import random

import factory  # type: ignore
from faker import Faker

from homework_30.homework.flaskr.models import Client, Parking, db


def is_true():
    return random.choice((True, False))


fake = Faker()


class ClientFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = Client
        sqlalchemy_session = db.session

    name = factory.Faker("first_name")
    surname = factory.Faker("last_name")
    car_number = factory.Faker("license_plate")
    credit_card = factory.LazyAttribute(
        lambda x: fake.credit_card_number() if is_true() else ""
    )


class ParkingFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = Parking
        sqlalchemy_session = db.session

    address = factory.Faker("address")
    opened = factory.Faker("pybool")
    count_places = factory.Faker("random_int", min=10, max=1000)
    count_available_places = factory.Faker("random_int", min=0, max=count_places)
