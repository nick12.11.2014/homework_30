import json
from datetime import datetime
from time import sleep

import pytest

from homework_30.homework.tests.factories import ClientFactory, ParkingFactory

new_client = {
    "car_number": "PIT",
    "credit_card": "1111 2222 3333 4444",
    "name": "Ivan",
    "surname": "Taranov",
}

new_parking = {
    "address": "Rostov",
    "count_available_places": 4,
    "count_places": 20,
    "opened": True,
}

new_client_parking = {
    "good_try": {"client_id": 1, "parking_id": 1},
    "no_credit_card": {"client_id": 2, "parking_id": 2},
    "no_free_places": {"client_id": 1, "parking_id": 3},
    "closed_parking": {"client_id": 5, "parking_id": 1},
    "exit_parking": {"client_id": 3, "parking_id": 2},
}


@pytest.mark.parametrize("url", ["/clients", "/clients/1", "/clients/2", "/clients/3"])
def test_get_clients(fill_all_tables, client, url):
    response = client.get(url)
    assert response.status_code == 200


def test_create_new_client(client):
    response = client.post("/clients", data=new_client)
    data = json.loads(response.data.decode("utf-8"))

    assert response.status_code == 201
    assert data["new_client"]["name"] == new_client["name"]
    assert data["new_client"]["surname"] == new_client["surname"]
    assert data["new_client"]["car_number"] == new_client["car_number"]
    assert data["new_client"]["credit_card"] == new_client["credit_card"]

    second_response = client.get(f"/clients/{data['new_client']['id']}")
    second_data = json.loads(second_response.data.decode("utf-8"))

    assert data["new_client"] == second_data["client"]


def test_create_new_client_with_factory(client):
    new_test_client = ClientFactory.create()
    response = client.post("/clients", data=new_test_client.to_json())
    data = json.loads(response.data.decode("utf-8"))

    assert response.status_code == 201
    assert data["new_client"]["name"] == new_test_client.name
    assert data["new_client"]["surname"] == new_test_client.surname
    assert data["new_client"]["car_number"] == new_test_client.car_number
    assert data["new_client"]["credit_card"] == new_test_client.credit_card

    second_response = client.get(f"/clients/{data['new_client']['id']}")
    second_data = json.loads(second_response.data.decode("utf-8"))

    assert data["new_client"] == second_data["client"]


def test_create_new_parking(client):
    response = client.post("/parking", data=new_parking)
    data = json.loads(response.data.decode("utf-8"))

    assert response.status_code == 201
    assert data["new_parking"]["address"] == new_parking["address"]
    assert (
        data["new_parking"]["count_available_places"]
        == new_parking["count_available_places"]
    )
    assert data["new_parking"]["count_places"] == new_parking["count_places"]
    assert data["new_parking"]["opened"] == new_parking["opened"]


def test_create_new_parking_with_factory(client):
    new_test_parking = ParkingFactory.create()
    response = client.post("/parking", data=new_test_parking.to_json())
    data = json.loads(response.data.decode("utf-8"))

    assert response.status_code == 201
    assert data["new_parking"]["address"] == new_test_parking.address
    assert (
        data["new_parking"]["count_available_places"]
        == new_test_parking.count_available_places
    )
    assert data["new_parking"]["count_places"] == new_test_parking.count_places
    assert data["new_parking"]["opened"] == new_test_parking.opened


def test_create_new_client_parking(fill_all_tables, client):
    response = client.post("/client_parking", data=new_client_parking["good_try"])
    data = json.loads(response.data.decode("utf-8"))

    assert response.status_code == 201
    assert (
        data["new_record"]["client_id"] == new_client_parking["good_try"]["client_id"]
    )
    assert (
        data["new_record"]["parking_id"] == new_client_parking["good_try"]["parking_id"]
    )

    second_response = client.post(
        "/client_parking", data=new_client_parking["good_try"]
    )
    second_data = json.loads(second_response.data.decode("utf-8"))

    assert second_response.status_code == 400
    assert second_data["error"] == (
        "According to our data, "
        "you have not left the previous parking lot yet."
        "If you are sure this is a mistake, "
        "think about it while searching a new parking."
    )


def test_create_new_client_parking_without_credit_card(fill_all_tables, client):
    response = client.post("/client_parking", data=new_client_parking["no_credit_card"])
    data = json.loads(response.data.decode("utf-8"))

    assert response.status_code == 400
    assert (
        data["error"] == "You need to add your credit "
        "card to use the parking services"
    )


def test_create_new_client_parking_without_free_places(fill_all_tables, client):
    response = client.post("/client_parking", data=new_client_parking["no_free_places"])
    data = json.loads(response.data.decode("utf-8"))

    assert response.status_code == 400
    assert data["error"] == "No free places"


def test_create_new_client_parking_with_closed_parking(fill_all_tables, client):
    response = client.post("/client_parking", data=new_client_parking["closed_parking"])
    data = json.loads(response.data.decode("utf-8"))

    assert response.status_code == 400
    assert data["error"] == "The parking is closed or not exists"


def test_exit_the_parking(fill_all_tables, client):
    sleep(1)
    response = client.delete("/client_parking", data=new_client_parking["exit_parking"])
    data = json.loads(response.data.decode("utf-8"))
    time_in = datetime.strptime(
        data["expired_record"]["time_in"], "%a, %d %b %Y %H:%M:%S %Z"
    )
    time_out = datetime.strptime(
        data["expired_record"]["time_out"], "%a, %d %b %Y %H:%M:%S %Z"
    )

    assert response.status_code == 201
    assert time_in < time_out
    assert data["expired_record"]["is_active"] is False

    second_response = client.post(
        "/client_parking", data=new_client_parking["exit_parking"]
    )
    second_data = json.loads(second_response.data.decode("utf-8"))

    assert response.status_code == 201
    assert (
        second_data["new_record"]["client_id"]
        == new_client_parking["exit_parking"]["client_id"]
    )
    assert (
        second_data["new_record"]["parking_id"]
        == new_client_parking["exit_parking"]["parking_id"]
    )
    assert second_data["new_record"]["is_active"] is True
