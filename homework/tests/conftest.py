import datetime

import pytest

from homework_30.homework.flaskr import create_app
from homework_30.homework.flaskr.models import Client, ClientParking, Parking, db

test_clients = [
    {
        "car_number": "BM111G",
        "credit_card": "9999 9999 9999 9999",
        "name": "Boris",
        "surname": "Moiseev",
    },
    {"car_number": "BM222G", "credit_card": "", "name": "Ivan", "surname": "Gamaz"},
    {
        "car_number": "BM222D",
        "credit_card": "1234 2345 3456 4567",
        "name": "Arkadiy",
        "surname": "Ukupnik",
    },
    {
        "car_number": "BM222U",
        "credit_card": "1234 2345 3456 4567",
        "name": "Valeriy",
        "surname": "Meladze",
    },
    {
        "car_number": "BM222D",
        "credit_card": "1234 2345 3456 4567",
        "name": "Leonid",
        "surname": "Agutin",
    },
]

test_parking = [
    {
        "address": "Kazan",
        "count_available_places": 5,
        "count_places": 75,
        "opened": False,
    },
    {
        "address": "Saint_petersburg",
        "count_available_places": 15,
        "count_places": 75,
        "opened": True,
    },
    {
        "address": "Moscow",
        "count_available_places": 0,
        "count_places": 75,
        "opened": True,
    },
]


@pytest.fixture()
def application():
    application = create_app()
    application.config["TESTING"] = True
    application.config["SQLALCHEMY_DATABASE_URI"] = "sqlite://"
    with application.app_context():
        yield application


@pytest.fixture()
def create_db_tables(application):
    db.create_all()
    db.session.flush()
    yield db
    db.session.close()
    db.drop_all()


@pytest.fixture()
def client(application):
    client_test = application.test_client()
    yield client_test


@pytest.fixture()
def fill_client_table(application, create_db_tables):
    for item in test_clients:
        client = Client(**item)
        create_db_tables.session.add(client)
    create_db_tables.session.commit()
    yield


@pytest.fixture()
def fill_parking_table(application, create_db_tables):
    for item in test_parking:
        parking = Parking(**item)
        create_db_tables.session.add(parking)
    create_db_tables.session.commit()
    yield


@pytest.fixture()
def fill_client_parking_table(application, create_db_tables):
    client = ClientParking(client_id=3, parking_id=2, time_in=datetime.datetime.now())
    create_db_tables.session.add(client)
    create_db_tables.session.commit()
    yield


@pytest.fixture()
def fill_all_tables(fill_client_table, fill_parking_table, fill_client_parking_table):
    yield
