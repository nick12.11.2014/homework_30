import os

from flask import Flask

from homework_30.homework.flaskr.models import db
from homework_30.homework.flaskr.view import bp


def create_app(test_config=None):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY="dev",
        DATABASE=os.path.join(app.instance_path, "hw29.db"),
        SQLALCHEMY_DATABASE_URI="sqlite:///hw29.db",
    )

    db.init_app(app)

    with app.app_context():
        db.create_all()

    app.register_blueprint(bp)

    return app
