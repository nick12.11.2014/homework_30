from typing import Any, Dict

from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class Client(db.Model):  # type: ignore
    __tablename__ = "client"

    id = db.Column(db.Integer, nullable=False, primary_key=True)
    name = db.Column(db.String(50), nullable=False)
    surname = db.Column(db.String(50), nullable=False)
    credit_card = db.Column(db.String(50), nullable=True)
    car_number = db.Column(db.String(10), nullable=True)

    @classmethod
    def get_clients(cls):
        return cls.query.all()

    @classmethod
    def get_client_by_id(cls, client_id: int):
        # May raise NoResultFound и MultipleResultsFound
        return cls.query.filter(cls.id == client_id).one()

    @classmethod
    def add_new_client(cls, data: Dict):
        new_client: Client = Client(**data)
        db.session.add(new_client)
        db.session.commit()
        return new_client

    @classmethod
    def is_available_credit_card(cls, client_id: int) -> bool:
        current_client: Client = db.session.query(cls).filter(cls.id == client_id).one()
        return current_client.credit_card != ""

    @classmethod
    def payment(cls, client_id: int, payment_sum: float):
        current_client: Client = cls.query.filter(cls.id == client_id).one()
        card_number = current_client.credit_card
        return (
            f"С вашей карты {card_number[:4]} **** **** {card_number[-4:]} "
            f"было списано {payment_sum} рублей. "
            f"Спасибо, что платите нам."
        )

    def to_json(self) -> Dict[str, Any]:
        return {item.name: getattr(self, item.name) for item in self.__table__.columns}


class Parking(db.Model):  # type: ignore
    __tablename__ = "parking"

    id = db.Column(db.Integer, nullable=False, primary_key=True)
    address = db.Column(db.String(100), nullable=False)
    opened = db.Column(db.Boolean, nullable=True)
    count_places = db.Column(db.Integer, nullable=False)
    count_available_places = db.Column(db.Integer, nullable=False)

    def to_json(self) -> Dict[str, Any]:
        return {item.name: getattr(self, item.name) for item in self.__table__.columns}

    @classmethod
    def is_available(cls, parking_id: int) -> bool:
        query = db.session.query(cls.count_available_places).filter(
            cls.id == parking_id, cls.opened == 1
        )
        result = query.one()[0]
        return result >= 1

    @classmethod
    def change_available_places_count(cls, parking_id: int):
        parking = cls.query.filter(cls.id == parking_id).one()
        parking.count_available_places -= 1
        return parking

    @classmethod
    def create_new_parking(cls, data: Dict):
        new_parking: Parking = Parking(**data)
        db.session.add(new_parking)
        db.session.commit()
        return new_parking


class ClientParking(db.Model):  # type: ignore
    __tablename__ = "client_parking"

    id = db.Column(db.Integer, nullable=False, primary_key=True)
    client_id = db.Column(db.Integer, nullable=True)
    parking_id = db.Column(db.Integer, nullable=True)
    time_in = db.Column(db.DateTime, nullable=True)
    time_out = db.Column(db.DateTime, nullable=True)
    is_active = db.Column(db.Boolean, nullable=False, default=True)
    __table_args__ = (
        db.UniqueConstraint(
            "client_id", "parking_id", "is_active", name="unique_client_parking"
        ),
    )

    @classmethod
    def create_record(cls, data: Dict):
        Parking.change_available_places_count(parking_id=data["parking_id"])
        new_record: ClientParking = ClientParking(**data)
        db.session.add(new_record)
        db.session.commit()
        return new_record

    @classmethod
    def make_record_expired(cls, data: Dict):
        record: ClientParking = cls.query.filter(
            cls.client_id == data["client_id"], cls.parking_id == data["parking_id"]
        ).one()
        record.time_out = data["time_out"]
        record.is_active = False
        current_parking: Parking = (
            db.session.query(Parking).filter(Parking.id == data["parking_id"]).one()
        )
        current_parking.count_available_places += 1
        print(Client.payment(client_id=data["client_id"], payment_sum=100))
        db.session.commit()
        return record

    def to_json(self) -> Dict[str, Any]:
        return {item.name: getattr(self, item.name) for item in self.__table__.columns}
