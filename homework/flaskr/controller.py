from typing import Dict

from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm.exc import MultipleResultsFound, NoResultFound

from homework_30.homework.flaskr.models import Client, ClientParking, Parking


def get_clients_list():
    results = Client.get_clients()
    return [item.to_json() for item in results]


def get_client_by_id(client_id: int):
    try:
        result = Client.get_client_by_id(client_id=client_id)
        return result.to_json()
    except NoResultFound:
        return f"There is no clients with id {client_id}"
    except MultipleResultsFound:
        return f"There is more than one client with id {client_id}"


def post_client(data: dict) -> Client:
    return Client.add_new_client(data=data)


def post_parking(data: dict):
    return Parking.create_new_parking(data=data)


def post_client_parking(data: Dict):
    try:
        is_available_parking: bool = Parking.is_available(parking_id=data["parking_id"])
    except NoResultFound:
        return "The parking is closed or not exists"
    except MultipleResultsFound as exc:
        print(exc)
        return "Internal error", 500
    if is_available_parking:
        try:
            is_available_credit_card: bool = Client.is_available_credit_card(
                client_id=data["client_id"]
            )
            if is_available_credit_card:
                try:
                    return ClientParking.create_record(data=data)
                except IntegrityError as exc:
                    print(exc)
                    return (
                        "According to our data, "
                        "you have not left the previous parking lot yet."
                        "If you are sure this is a mistake, "
                        "think about it while searching a new parking."
                    )
            else:
                return "You need to add your credit card " "to use the parking services"
        except NoResultFound:
            return f'No client with id {data["client_id"]}'
        except MultipleResultsFound as exc:
            print(exc)
            return "Internal error", 500
    else:
        return "No free places"


def make_client_parking_expired(data: Dict):
    return ClientParking.make_record_expired(data=data)
