import datetime

from flask import Blueprint, jsonify, request

from homework_30.homework.flaskr.controller import (
    get_client_by_id,
    get_clients_list,
    make_client_parking_expired,
    post_client,
    post_client_parking,
    post_parking,
)

bp = Blueprint("main", __name__)


@bp.route("/clients", methods=["GET", "POST"])
def clients():
    if request.method == "GET":
        results = get_clients_list()
        return jsonify(clients=results), 200
    else:
        form = request.form
        new_client = post_client(data=form)
        return jsonify(new_client=new_client.to_json()), 201


@bp.route("/clients/<int:client_id>", methods=["GET"])
def clients_by_id(client_id: int):
    result = get_client_by_id(client_id=client_id)
    return jsonify(client=result), 200


@bp.route("/parking", methods=["POST"])
def parking():
    data = request.form.to_dict()
    if data["opened"].lower() in ("false", "0"):
        data["opened"] = False
    else:
        data["opened"] = True
    new_parking = post_parking(data=data)
    return jsonify(new_parking=new_parking.to_json()), 201


@bp.route("/client_parking", methods=["POST", "DELETE"])
def client_parking():
    data = request.form.to_dict()
    if request.method == "POST":
        data["time_in"] = datetime.datetime.now()
        new_client_parking = post_client_parking(data=data)
        try:
            return jsonify(new_record=new_client_parking.to_json()), 201
        except AttributeError:
            return jsonify(error=new_client_parking), 400
    else:
        data["time_out"] = datetime.datetime.now()
        expired_record = make_client_parking_expired(data=data)
        try:
            print(expired_record)
            return jsonify(expired_record=expired_record.to_json()), 201
        except (AttributeError, TypeError):
            return jsonify(error=expired_record), 400
